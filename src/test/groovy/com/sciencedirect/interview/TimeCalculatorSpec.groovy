package com.sciencedirect.interview

import spock.lang.Specification
import spock.lang.Unroll

class TimeCalculatorSpec extends Specification{
 
    @Unroll
    def "should add date strings properly" () {

        given: "A TimeCalculatorInstance"
        TimeCalculator calculator = new TimeCalculator(strategy, withDaysCarriage)

        when: "Adding two time strings"
        def result = calculator.sum(firstString, secondString)

        then: "We have the expected result"
        result == expectedResult

        where:
        firstString | secondString | expectedResult  | withDaysCarriage | strategy
        "12:50:03"  | "02:00:50"   | "14:50:53"      | false            | StrategyType.SECONDS
        "12:50:03"  | "12:00:50"   | "1:00:50:53"    | true             | StrategyType.SECONDS
        "12:50:03"  | "12:00:50"   | "00:50:53"      | false            | StrategyType.SECONDS
        "12:50:03"  | "02:00:50"   | "14:50:53"      | false            | StrategyType.CARRIAGE
        "12:50:03"  | "12:00:50"   | "1:00:50:53"    | true             | StrategyType.CARRIAGE
        "12:50:03"  | "12:00:50"   | "00:50:53"      | false            | StrategyType.CARRIAGE
    }

    @Unroll
    def "should properly validate input strings" () {
        given: "A TimeCalculatorInstance"
        TimeCalculator calculator = new TimeCalculator()

        when: "Providing strings in the wrong format"
        def result = calculator.sum(firstString, secondString)

        then: "An exception is thrown"
        def exception = thrown(IllegalArgumentException)
        exception.message == "Incorrect time string format provided:HH:mm:ss"

        where:
        firstString   | secondString
        "12;50;03"    | "02;00;50"
        "24:60:00"    | "02:00:50"
        "-12:-50:-03" | "-02:-00:-50"
        "23:50"       | "13"
        null          | ""
        "lizards"     | "&wizards"
    }
}