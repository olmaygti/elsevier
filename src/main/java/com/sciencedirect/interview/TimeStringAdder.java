package com.sciencedirect.interview;

import java.util.List;

public interface TimeStringAdder {
    public String sum(List<Integer> addend1, List<Integer> addend2);

    default String format(Integer hours, Integer minutes, Integer seconds, boolean daysOverflow) {
        StringBuilder result = new StringBuilder();
        if (daysOverflow) {
            result.append("1:");
        }

        return result.append(String.format("%02d:%02d:%02d", hours, minutes, seconds)).toString();
    }
}