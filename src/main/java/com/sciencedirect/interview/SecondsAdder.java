package com.sciencedirect.interview;

import java.util.*;

public class SecondsAdder implements TimeStringAdder {

    private Boolean daysOverflowAware;

    public SecondsAdder() {
        this(false);
    }

    public SecondsAdder(Boolean daysOverflowAware) {
        super();
        this.daysOverflowAware = daysOverflowAware;
    }

    public String sum(List<Integer> addend1, List<Integer> addend2) {
        return secondsToTimeString(toSeconds(addend1) + toSeconds(addend2));
    }

    private String secondsToTimeString(Integer totalSeconds) {
        final Integer seconds = totalSeconds % 60;
        totalSeconds /= 60;
        final Integer minutes = totalSeconds % 60;
        totalSeconds /= 60;
        final Integer hours = totalSeconds % 24;
        Boolean daysOverflow = (totalSeconds / 24) >= 1;

        return format(hours, minutes, seconds, daysOverflow && daysOverflowAware);
    }

    private Integer toSeconds(List<Integer> timeRepr) {
        final int[] weights = {3600, 60, 1};
        final int[] idx = { 0 };
        return timeRepr.stream()
            .reduce(0, (value, accum) -> value + (accum * weights[idx[0]++]));
    }
}