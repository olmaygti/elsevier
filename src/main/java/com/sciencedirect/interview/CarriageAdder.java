package com.sciencedirect.interview;

import org.apache.commons.lang3.tuple.MutablePair;
import java.util.*;

public class CarriageAdder implements TimeStringAdder {

    private Boolean daysOverflowAware;

    public CarriageAdder() {
        this(false);
    }

    public CarriageAdder(Boolean daysOverflowAware) {
        super();
        this.daysOverflowAware = daysOverflowAware;
    }

    public String sum(List<Integer> addend1, List<Integer> addend2) {
        MutablePair<Integer, Boolean> secondsResult =
            addWithCarriage(addend1.get(2), addend2.get(2), 60, false);

        MutablePair<Integer, Boolean> minutesResult =
            addWithCarriage(addend1.get(1), addend2.get(1), 60, secondsResult.getRight());

        MutablePair<Integer, Boolean> hoursResult =
            addWithCarriage(addend1.get(0), addend2.get(0), 24, minutesResult.getRight());

        return format(hoursResult.getLeft(),
                minutesResult.getLeft(), secondsResult.getLeft(),
                this.daysOverflowAware &&  hoursResult.getRight());
    }

    private MutablePair<Integer, Boolean> addWithCarriage(
            final Integer addend1, final Integer addend2, final Integer base, Boolean carriageCarried) {

        MutablePair<Integer, Boolean> toReturn = new MutablePair();

        Integer result  = addend1 + addend2;
        Boolean carriage = false;

        if (carriageCarried) {
            result += 1;
        }

        if (result >= base) {
            result %= base;
            carriage = true;
        }

        toReturn.setLeft(result);

        return MutablePair.<Integer, Boolean>of(result, carriage);
    }
}