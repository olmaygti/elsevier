package com.sciencedirect.interview;

import java.util.*;
import java.util.stream.*;

public class TimeCalculator {

    private TimeStringAdder worker;

    public TimeCalculator () {
        this(StrategyType.SECONDS, false);
    }

    public TimeCalculator(StrategyType strategy, Boolean daysOverflowAware) {
        try {
            this.worker = (TimeStringAdder)(strategy.getImpl()
                .getDeclaredConstructor(Boolean.class).newInstance(daysOverflowAware));
        } catch(Exception ex) {
            throw new RuntimeException("Failed to create strategy worker: " + ex.getCause());
        }
    }

    public String sum(String a, String b) {
        List<Integer> firstStringValues = parseTimeString(a);
        List<Integer> secondStringValues = parseTimeString(b);

        return this.worker.sum(firstStringValues, secondStringValues);
    }


    private List<Integer> parseTimeString(String timeString) {
        List<Integer> values = null;
        Boolean valid = timeString != null;
        if (valid) {
            try {
                values = Arrays.stream(timeString.split(":"))
                    .map(Integer::parseInt)
                    .collect(Collectors.toList());
            } catch(NumberFormatException ex) {
                valid = false;
            }
        }

        valid = valid && values.size() == 3 && maxPositive(values.get(0), 23) && maxPositive(values.get(1), 59) && maxPositive(values.get(2), 59);

        if (!valid) {
            throw new IllegalArgumentException("Incorrect time string format provided:HH:mm:ss");
        }
        return values;
    }

    private boolean maxPositive(Integer num, Integer max) {
        return num >= 0 && num <= max;
    }
}

enum StrategyType {
    CARRIAGE(CarriageAdder.class),
    SECONDS(SecondsAdder.class);

    private final Class implementation;

    private StrategyType(Class implementation) {
        this.implementation = implementation;
    }

    public Class getImpl() {
        return this.implementation;
    }
}
