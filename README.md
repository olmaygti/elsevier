Interview - Time Calculator
===========================

Given two time strings, calculate their time sum.

Example:
  12:50:03 + 02:00:50 = 14:50:53




Interview - Time Calculator - Solution
======================================

At the begining I must admit I was was a bit confused and surprised when I saw the contents of the project. With specification so simple and undetailed, there are some implementations that could be accomplished with very few lines of code using a bunch of classes for the JDK.

I'm not sure if I made the correct call here, but I decided to "complicate" the solution a little bit.

First of all, I upgraded the gradle version, and replaced the JUnit dependencies with Spock. As all src files for the code were provided in Java, but no one was provided for tests, I decided to code them with this fantastic framework, and my favourite language: Groovy.

I don't find any advantage of using both maven and gradle at the same time (except for developers that doesn't have one installed could use the other), so I didn't touched/include the pom.xml file I was provided.

Same thing with .iml files. I don't have a personal IntelliJ license (and I usually avoid using IDE's unless the project gets quite big), so I didn't include those files as part of this solution.

For the solution itself I chose to implement a couple of different algorithms to add the time strings, using a simple implementation of the StrategyPattern. I know this is overkilling for such a simple test. In fact I would say this solution matches perfectly the **Golden Hammer AntiPattern**.

I usually like to follow the **KISS** and **Don't abstract too soon** principles, and this is the perfect example of an overdesigned solution, which I will never choose to go with in a real environment.

But for the shake of this interview, I though implementing it this way would help me to present me as a software engineer: providing different solutions to solve the same problem with a mantainable and easy to extend design, as well as showing my knowledge about Designn Patterns (I almost implemented the FactoryPattern to instantiate the different strategy implementations, but finally I thought that way too much for this example).

This was also a perfect oportunity for me to play around with Java8. I've been reading about it's new features way before it was actually released, but never did anything with it until now. I used the streams API and a few lambdas, as well as default methods implementation in interfaces.

Both algorithms are quite simple:

* **CarriageAdder** just add each of the members of the string one by one, taking into consideration if that sum exceeds the base it's using (24, or 60). Just as we learned in school.
*  **SecondsAdder** uses another approach: it first converts all the members of the time strings to seconds and then add them using that unit, to then unroll the operation over the result and transform it again to a time string.

The sums can be run with or without overflowing days if the addition of hours exceeds 24 by providing a boolean parameter when creating the TimeCalculator instance (which defaults to false).

Just `gradle test` or `gradle run` and enjoy, hope you like it!!.